def navbar_clubs(request):
    contexte = {}
    if request.user.is_authenticated:
        pseudo = request.user.account.firstname
        clubs = request.user.account.clubs.all()
        pk = request.user.account.selected_club
        if clubs.exists():
            selected_club = clubs.get(id=pk)
            contexte.update({"selected_club":selected_club})


        contexte.update({"clubs": clubs, "pseudo":pseudo})
    return contexte

    