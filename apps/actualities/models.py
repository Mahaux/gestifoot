from django.db import models
from apps.accounts.models import Club


class Actuality(models.Model):
    TYPES = (("Information", "Information"), ("Fete", "Fete"), ("Match", "Match"))
    title = models.CharField(max_length=50, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    statut = models.CharField(max_length=50, choices=TYPES, blank=True, null=True)
    date_posted = models.DateTimeField(auto_now_add=True)
    fk_club = models.ForeignKey(Club, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["-date_posted"]
