from django.forms import ModelForm
from django import forms
from .models import Actuality


class Actuality_form(ModelForm):
    class Meta:
        model = Actuality
        fields = "__all__"
        labels = {"title": "Titre :", "content": "Contenu :"}
        exclude = ["date_posted", "fk_club"]
