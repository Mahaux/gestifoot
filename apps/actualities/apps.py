from django.apps import AppConfig


class ActualitiesConfig(AppConfig):
    name = "actualities"
