from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from apps.accounts.models import *
from apps.actualities.models import Actuality
from .forms import Actuality_form


@login_required(login_url="login")
def home(request, pk=0):
    clubs = request.user.account.clubs.all()
    contexte = {"title": "Home Page"}
    if pk == 0:
        if clubs.exists():
            club = clubs.first()
            setattr(request.user.account, 'selected_club', club.id)
            request.user.account.save()
            actuality_list = Actuality.objects.filter(fk_club=club.id)
            contexte.update({"actuality_list": actuality_list})
    else:
        club = clubs.get(id=pk)
        setattr(request.user.account, 'selected_club', club.id)
        request.user.account.save()
        actuality_list = Actuality.objects.filter(fk_club=club.id)
        contexte.update({"actuality_list": actuality_list})

    return render(request, "actualities/home.html", contexte)


@login_required(login_url="login")
def create_actuality(request):
    if request.method == "POST":
        form = Actuality_form(request.POST)
        if form.is_valid():
            form.instance.fk_club = request.user.account.fk_account
            form.save()
            return redirect("home")
    else:
        form = Actuality_form()

    contexte = {"title": "Actuality Page", "form": form}
    return render(request, "actualities/create_actuality.html", contexte)


@login_required(login_url="login")
def update_actuality(request, pk):
    actu = Actuality.objects.get(id=pk)
    if request.method == "POST":
        form = Actuality_form(request.POST, instance=actu)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = Actuality_form(instance=actu)

    contexte = {"title": "Actuality Page", "form": form}
    return render(request, "actualities/update_actuality.html", contexte)


@login_required(login_url="login")
def delete_actuality(request, pk):
    actu = Actuality.objects.get(id=pk)
    if request.method == "POST":
        actu.delete()
        return redirect("home")

    contexte = {"title": "Actuality Page", "actu": actu}
    return render(request, "actualities/delete_actuality.html", contexte)
