from django.urls import path
from apps.actualities import views

urlpatterns = [
    path("home/", views.home, name="home"),
    path("home/<int:pk>/", views.home, name="home"),
    path("create/", views.create_actuality, name="create_actu"),
    path("update/<int:pk>/", views.update_actuality, name="update_actu"),
    path("delete/<int:pk>/", views.delete_actuality, name="delete_actu"),
]