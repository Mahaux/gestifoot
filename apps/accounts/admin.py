from django.contrib import admin
from .models import *

admin.site.register(Account)
admin.site.register(Coach)
admin.site.register(Player)
admin.site.register(Fan)
admin.site.register(Club)
admin.site.register(Affiliation)
