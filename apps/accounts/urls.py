from django.urls import path
from . import views

urlpatterns = [
    path("profile/", views.profile, name="profile"),
    path("register/", views.register_user, name="register_user"),
    path("register_club/", views.register_club, name="register_club"),
    path("list_clubs/", views.list_clubs, name="list_clubs"),
    path("details_club/<int:pk>/", views.details_club, name="details_club"),
]