from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import *


class Account_profile(ModelForm):
    class Meta:
        model = Account
        fields = "__all__"
        labels = {
            "picture": "Photo de profil :",
            "firstname": "Prénom :",
            "lastname": "Nom :",
            "email": "Adresse mail :",
            "phone": "Téléphone :",
        }
        widgets = {
            "picture": forms.FileInput(),
        }
        exclude = ["statut", "selected_club", "fk_user"]


class Create_user(UserCreationForm):
    class Meta:
        model = User
        fields = [
            "username",
            "password1",
            "password2",
        ]


class Create_club(UserCreationForm):
    class Meta:
        model = User
        fields = ["username", "password1", "password2"]
        labels = {
            "username": "Matricule :",
        }


class Club_details(ModelForm):
    class Meta:
        model = Account
        fields = ["firstname", "email", "phone"]
        labels = {
            "firstname": "Nom :",
            "email": "Adresse mail :",
            "phone": "Téléphone :",
        }
        widgets = {
            "firstname": forms.TextInput(attrs={"disabled": True}),
            "email": forms.TextInput(attrs={"disabled": True}),
            "phone": forms.TextInput(attrs={"disabled": True}),
        }



