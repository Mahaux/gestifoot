from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from .models import *
from .forms import *
from .decorators import unauthenticated_user


@unauthenticated_user
def register_user(request):
    if request.method == "POST":
        form = Create_user(request.POST)
        if form.is_valid():
            user = form.save()

            Account.objects.create(statut=("People", "People"), fk_user=user)

            messages.success(request, "Account was created for " + user.username)
            return redirect("login")
    else:
        form = Create_user()

    contexte = {"form": form}
    return render(request, "accounts/register_user.html", contexte)


def register_club(request):
    if request.method == "POST":
        form = Create_club(request.POST)
        if form.is_valid():
            user_club = form.save()

            account_club = Account.objects.create(
                statut=("Club", "Club"), fk_user=user_club
            )

            Club.objects.create(fk_account=account_club)

            messages.success(request, "Club : " + name + " was created")
            return redirect("login")
    else:
        form = Create_club()

    contexte = {"form": form}
    return render(request, "accounts/register_club.html", contexte)


# @unauthenticated_user
def login(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("actualities/home")
        else:
            messages.info(request, "Username or password is incorrect")
    contexte = {}
    return render(request, "accounts/login.html", contexte)


def logout(request):
    logout(request)
    return redirect("login")


def profile(request):
    if request.method == "POST":
        form_account = Account_profile(
            request.POST, request.FILES, instance=request.user.account
        )
        if form_account.is_valid():
            form_account.save()
            return redirect("profile")
    else:
        form_account = Account_profile(instance=request.user.account)

    context = {"form_account": form_account}
    return render(request, "accounts/profile.html", context)


def list_clubs(request):
    my_clubs = request.user.account.clubs.all()
    list_clubs = Club.objects.all()

    context = {"list_clubs": list_clubs, "my_clubs": my_clubs}
    return render(request, "accounts/list_clubs.html", context)


def details_club(request, pk):
    club = Club.objects.get(id=pk)
    my_poste = Affiliation.objects.filter(fk_account=request.user.account).filter(poste="Joueur")
    if request.method == "POST":
        if "follow" in request.POST:
            Affiliation.objects.create(
                fk_account=request.user.account, fk_club=club, poste="Fan"
            )
        elif "affiliate_player" in request.POST:
            Affiliation.objects.create(
                fk_account=request.user.account, fk_club=club, poste="Joueur"
            )
        elif "affiliate_coach" in request.POST:
            Affiliation.objects.create(
                fk_account=request.user.account, fk_club=club, poste="Entraineur"
            )
        return redirect("home")
    else:
        form = Club_details(instance=club.fk_account)

    context = {"club": club, "my_poste": my_poste, "form": form}
    return render(request, "accounts/details_club.html", context)
