from django.db import models
from django.contrib.auth.models import User


class Account(models.Model):
    TYPES = (("Club", "Club"), ("People", "People"))
    picture = models.ImageField(default="default.jpg", blank=True, null=True)
    firstname = models.CharField(max_length=50, blank=True, null=True)
    lastname = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(max_length=200, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    statut = models.CharField(
        max_length=10,
        choices=TYPES,
        blank=True,
        null=True,
    )
    fk_user = models.OneToOneField(
        User, on_delete=models.CASCADE, blank=True, null=True
    )

    selected_club = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.firstname + " (" + self.statut + ")"


class Coach(models.Model):
    fk_account = models.OneToOneField(
        Account, on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        return self.fk_account.firstname


class Player(models.Model):
    fk_account = models.OneToOneField(
        Account, on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        return self.fk_account.firstname


class Fan(models.Model):
    fk_account = models.OneToOneField(
        Account, on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        return self.fk_account.firstname


class Club(models.Model):
    fk_account = models.OneToOneField(
        Account,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="club",
    )
    people = models.ManyToManyField(
        Account, through="Affiliation", related_name="clubs"
    )

    def __str__(self):
        return self.fk_account.firstname


class Affiliation(models.Model):
    POSTES = (
        ("Fan", "Fan"),
        ("Entraineur", "Entraineur"),
        ("Joueur", "Joueur"),
        ("Club", "Club"),
    )

    fk_account = models.ForeignKey(
        Account, on_delete=models.CASCADE, blank=True, null=True
    )
    fk_club = models.ForeignKey(Club, on_delete=models.CASCADE, blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    poste = models.CharField(max_length=20, choices=POSTES, blank=True, null=True)

    def __str__(self):
        return (
            self.fk_account.firstname
            + ":"
            + self.fk_club.fk_account.firstname
            + ":"
            + self.poste
        )

    class Meta:
        unique_together = [["fk_account", "fk_club"]]
